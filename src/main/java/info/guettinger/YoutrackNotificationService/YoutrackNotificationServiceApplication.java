package info.guettinger.YoutrackNotificationService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YoutrackNotificationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(YoutrackNotificationServiceApplication.class, args);
	}

}
